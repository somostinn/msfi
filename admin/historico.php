<?php

defined('ABSPATH') or die( "Bye bye" );

//Comprueba que tienes permisos para acceder a esta pagina
if (! current_user_can ('manage_options')) wp_die (__ ('No tienes suficientes permisos para acceder a esta página.'));

		 global $wpdb;
		$tabla = $wpdb->prefix . "msfi_historico";
		$historicos = $wpdb->get_results("SELECT * FROM $tabla");
		
		$tabla = '<table class="wp-list-table widefat fixed striped"> ';
		$tabla = $tabla . '<thead><tr><th width="30%">Fecha</th><th>MSFIA</th><th>MSFIB</th></tr></thead> ';
		$tabla = $tabla . '<tbody id="the-list"> ';
		$tabla_f ='';
		foreach ($historicos as $historico) {
			$fecha = esc_textarea($historico->fecha);
			$msfia= esc_textarea($historico->msfia);
			$msfib = esc_textarea($historico->msfib);			
			$tabla_f = $tabla_f . "<tr><td>$fecha</td><td>$msfia</td><td>$msfib</td></tr>";
		}
		$tabla = $tabla . '</tbody></table></div>';


?>
	<div class="wrap">
		<h2><?php _e( 'Acciones de MSFI' ) ?></h2>
		Historico
<?php
		echo($tabla);
?>

	</div>
