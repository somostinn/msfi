<?php
/*
Plugin Name: Acciones Mercantil SFI
Plugin URI: https://msfi.com
Description: Valor de las acciones MSFIA y MSFIB en la Bolsa de Valores de Panama
Version: 1.0
Author: AngeIng. Oscar Daniel Velasquez
Author URI: https://oscar.somostinn.com
License: GPL2
*/

defined('ABSPATH') or die("Bye bye");

define('TINN_RUTA', plugin_dir_path(__FILE__));
define('TINN_NOMBRE','MSFI');


register_activation_hook(__FILE__, 'TINN_msfi_init');
/**
 * Realiza las acciones necesarias para configurar el plugin cuando se activa
 *
 * @return void
 */
function TINN_msfi_init()
{
    global $wpdb;
    $tabla = $wpdb->prefix . "msfi_historico";
    $charset_collate = $wpdb->get_charset_collate();
    $query = "CREATE TABLE IF NOT EXISTS $tabla (
		id MEDIUMINT(9) NOT NULL AUTO_INCREMENT,
		fecha VARCHAR(40) NOT NULL,
		msfia VARCHAR(40),
		msfib VARCHAR(40),
		created_at datetime NOT NULL,
		UNIQUE (id)
	) $charset_collate;";

    include_once ABSPATH . 'wp-admin/includes/upgrade.php';
    dbDelta($query);
}


//Archivos externos
include(TINN_RUTA.'/includes/opciones.php');