<?php

defined('ABSPATH') or die( "Bye bye" );

/*
 * Nuevo menu de administrador
 */
 
// El hook admin_menu ejecuta la funcion rai_menu_administrador
add_action( 'admin_menu', 'tinn_menu_administrador' );
 
// Top level menu del plugin
function tinn_menu_administrador()
{
	add_menu_page(TINN_NOMBRE,TINN_NOMBRE,'manage_options',TINN_RUTA . '/admin/configuracion.php'); //Crea el menu
    add_options_page(TINN_NOMBRE,TINN_NOMBRE, 'manage_options', 'tinn', 'tinn_options'); //Crea la pagina de opciones
    add_submenu_page(TINN_RUTA . '/admin/configuracion.php','Historico','Historico','manage_options',TINN_RUTA . '/admin/historico.php');
}
 ?>